# PROJECT COMPUTER V1

Trello:         <https://trello.com/b/CS83SUSu/computer-v1>
Gitignore ref:  <https://github.com/github/gitignore>
Modules used:
  GnuReadline:  <https://github.com/ludwigschwardt/python-gnureadline>

## Launch

`python main.py`
or
`python3 main.py`


### Notes
home shebang : #!/usr/local/bin/python3.7
school shebang : #!/Users/jmonneri/.brew/bin/python3.7
look behind look ahead