#!/Users/jmonneri/.brew/bin/python3.7
#!/usr/local/bin/python3.7
# *************************************************************************** #
#                                                           LE - /            #
#                                                               /             #
#    main.py                                          .::    .:/ .      .::   #
#                                                  +:+:+   +:    +:  +:+:+    #
#    By: jmonneri <jmonneri@student.le-101.fr>      +:+   +:    +:    +:+     #
#                                                  #+#   #+    #+    #+#      #
#    Created: 2019/05/22 13:31:06 by jmonneri     #+#   ##    ##    #+#       #
#    Updated: 2019/05/22 13:31:06 by jmonneri    ###    #+. /#+    ###.fr     #
#                                                          /                  #
#                                                         /                   #
# *************************************************************************** #


import gnureadline
import re
import pystache
import json
from polynomialEquation import Poly
from samples import regexes, printPhrases


def output(poly):
    #print(json.dumps(poly.__dict__, indent=2))
    # Reduced Form
    reducedForm = 'Reduced Form:\t\t'
    begin = 1
    for key, value in poly.power_left.items():
        if (begin):
            sign = '-' if value < 0 else ''
            begin = 0
        else:
            sign = '- ' if value < 0 else '+ '
        reducedForm += sign + str(abs(value)).rstrip('0').rstrip('.') + ' * X^' + str(key) + ' '
    print(reducedForm + '= 0')

    # Polynomial Degree
    print('Polynomial Degree:\t%s' % poly.polynomialDegree)

    # Errors
    if (poly.error):
        print('\033[91m' + poly.error + '\033[00m')
        return
    values = {
        'a': poly.a.rstrip('0').rstrip('.'),
        'b': poly.b.rstrip('0').rstrip('.'),
        'c': poly.c.rstrip('0').rstrip('.'),
        'aAbs': str(abs(float(poly.a))).rstrip('0').rstrip('.'),
        'bAbs': str(abs(float(poly.b))).rstrip('0').rstrip('.'),
        'cAbs': str(abs(float(poly.c))).rstrip('0').rstrip('.'),
        'aSign': '-' if float(poly.a) < 0 else '+',
        'bSign': '-' if float(poly.b) < 0 else '+',
        'cSign': '-' if float(poly.c) < 0 else '+',
    }

    # Reduced Natural Form
    reducedNaturalForm = "Reduced Natural Form:\t"
    begin = 1
    if (float(poly.a) != 0):
        begin = 0
        aSign = '-' if float(poly.a) < 0 else ''
        reducedNaturalForm += values['a'] + 'x^2 ' if values['aAbs'] != '1' else aSign + 'x^2 ' 
    if (float(poly.b) != 0):
        bSign = values['bSign'] + ' '
        if (begin):
            begin = 0
            bSign = '-' if float(poly.b) < 0 else ''
        reducedNaturalForm += bSign + values['bAbs'] + 'x ' if values['bAbs'] != '1' else bSign + 'x '
    if (float(poly.c) != 0):
        reducedNaturalForm += values['cSign'] + ' ' + values['cAbs'] + ' '
    print(reducedNaturalForm + '= 0')

    # Solutions
    if (poly.polynomialDegree == 1):
        print("The solution is:\t%s" % poly.x1)
    else:
        print("Discriminant:\t\t%s" % poly.delta.rstrip('0').rstrip('.'))
        if (float(poly.delta) > 0):
            print("Discriminant is strictly positive, the two solutions are:")
            print("x1 = ", poly.x1)
            print("x2 = ", poly.x2)
        elif (float(poly.delta) < 0):
            print("Discriminant is strictly negative, there is not real solution but two irreals:")
            print("x1 = ", poly.x1)
            print("x2 = ", poly.x2)
        else:
            print("Discriminant is null, the solution is:")
            print("x = ", poly.x1)


def get_calc(matchObj):
    groups = matchObj.groups()
    if (len(groups) != 4):
        calcString = "".join(groups)
    else:
        calcString = groups[0] + groups[2] + groups[3]
    calc = str(eval(calcString.replace("^", "**")))
    if (len(groups) != 4):
        return(calc)
    return(calc + groups[1])


def xMultToPower(matchObj):
    return('X^%d' % matchObj.groups()[0].count('X'))


def format_equation(string):
    if (re.match(regexes["goodChars"], string) is None):
        return(None)
    string = string.upper()
    string = string.replace(" ", "")
    # XX | X*X => X^2
    while(re.search("(X(?:\*?X)+)", string)):
        string = re.sub("(X(?:\*?X)+)", xMultToPower, string)
    # X^1 => 1*X^1
    string = re.sub("(?:(?<=[^\*\/\d])|(?<=^))X", r"1*X", string)
    # X => X^1
    string = re.sub("X(?!\^)", r"X^1", string)
    # aX => a*X
    string = re.sub("(\d+(?:\.\d+)?)X", r"\1*X", string)
    # Simple calcs
    while(re.search("(\d+(?:\.\d+)?)(\*X\^\d+)([%/*^])(\d+(?:\.\d+)?)", string)):
        string = re.sub("(\d+(?:\.\d+)?)(\*X\^\d+)([%/*^])(\d+(?:\.\d+)?)", get_calc, string)  # check regex
    while(re.search("(\d+(?:\.\d+)?[*/%^]\d+(?:\.\d+)?)", string)):
        string = re.sub("(\d+(?:\.\d+)?[*/%^]\d+(?:\.\d+)?)", get_calc, string)  # check regex
    # 4 => 4*X^0
    string = re.sub("(?<![\^*/%])((?:\d+(?:\.\d+)?[*/%]?)+)(?![X\d*/%^.]|\*X)", r"\1*X^0", string)
    return(string)


def main():
    print("Welcome in CPV1!")
    while (True):
        equation = input("\n\033[96mEnter your equation or ask to quit:\033[00m\n\n")
        if (re.search(regexes["exit"], equation, flags=re.IGNORECASE)):
            break
        else:
            print(equation)
            equation = format_equation(equation)
            if (equation is None):
                print("\n\033[91mYour entry is not a good equation (Bad Characters)\033[00m")
                continue
            poly = Poly(equation)
            output(poly)


if __name__ == "__main__":
    main()

#def prRed(skk): print("\033[91m {}\033[00m" .format(skk)) 
#def prGreen(skk): print("\033[92m {}\033[00m" .format(skk)) 
#def prYellow(skk): print("\033[93m {}\033[00m" .format(skk)) 
#def prLightPurple(skk): print("\033[94m {}\033[00m" .format(skk)) 
#def prPurple(skk): print("\033[95m {}\033[00m" .format(skk)) 
#def prCyan(skk): print("\033[96m {}\033[00m" .format(skk)) 
#def prLightGray(skk): print("\033[97m {}\033[00m" .format(skk)) 
#def prBlack(skk): print("\033[98m {}\033[00m" .format(skk)) 