# *************************************************************************** #
#                                                           LE - /            #
#                                                               /             #
#    polynomialEquation.py                            .::    .:/ .      .::   #
#                                                  +:+:+   +:    +:  +:+:+    #
#    By: jojomoon <jojomoon@student.le-101.fr>      +:+   +:    +:    +:+     #
#                                                  #+#   #+    #+    #+#      #
#    Created: 2019/07/01 18:02:50 by jojomoon     #+#   ##    ##    #+#       #
#    Updated: 2019/07/02 22:55:41 by jojomoon    ###    #+. /#+    ###.fr     #
#                                                          /                  #
#                                                         /                   #
# *************************************************************************** #

import re
import array
import json


class Poly:

    nbInst = 0

    def __init__(self, eq):
        Poly.nbInst += 1
        self.eq = eq
        self.power_left = dict()
        self.power_right = dict()
        self.delta = None
        self.a = None
        self.b = None
        self.c = None
        self.x1 = None
        self.x2 = None
        self.error = None
        self.msc = 0
        self.polynomialDegree = 0
        self.generate_object()

    def __del__(self):
        Poly.nbInst -= 1

    def __repr__(self):
        return(json.dumps(self.__dict__, indent=2))

    def __str__(self):
        return(json.dumps(self.__dict__, indent=2))

    def get_powers(self, string, sidePowers):
        match_tab = re.findall("((-?\d+(?:\.\d+)?)\*X\^(\d+))", string)
        for match in match_tab:
            if (int(match[2]) in sidePowers):
                sidePowers[int(match[2])] += float(match[1])
            else:
                sidePowers[int(match[2])] = float(match[1])

    def get_divided(self, string, sidee):
        match_tab = re.findall("((-?\d+(?:\.\d+)?)\/X\^(\d+))", string)
        for match in match_tab:
            mult = int(match[2]) - self.msc
            self.msc = mult + self.msc if mult > 0 else self.msc
            for side in self.power_left, self.power_right:
                keys = sorted(list(side.keys()), reverse=True)
                for power in keys:
                    if (mult > 0):
                        side[power + mult] = side[power]
                        del side[power]
                if (side == self.power_left):
                    add = float(match[1]) if sidee == "left" else -float(match[1])
                    new_pow = 0 if mult >= 0 else -mult
                    side[new_pow] = side[new_pow] + add if new_pow in side else add

    def reduce_equation(self):
        for power, value in self.power_right.items():
            if (power in self.power_left):
                self.power_left[power] -= value
            else:
                self.power_left[power] = -value

    def test_powers(self):
        for key in self.power_left.keys():
            if (self.polynomialDegree < key):
                self.polynomialDegree = key
        if (self.polynomialDegree > 2):
            self.error = "It's not a 2nd degree equation"
            return(False)
        return(True)

    def set_a_b_c(self):
        self.a = str(self.power_left[2]) if 2 in self.power_left.keys() else "0"
        self.b = str(self.power_left[1]) if 1 in self.power_left.keys() else "0"
        self.c = str(self.power_left[0]) if 0 in self.power_left.keys() else "0"

    def calc_and_irrationnal_fraction(self, b, delta, a, sign):
        ret = ''
        up = eval('%s %s (%s ** 0.5)' % (b, sign, delta)) 
        down = eval('2 * ' + a)
        result = str(eval(str(up) + ' / ' + str(down))).rstrip('0').rstrip('.')
        if ('.' in result):
            if (up.is_integer() and down.is_integer()):
                # PGCD
                a, b = up, down
                tmp = a % b
                while (tmp != 0):
                    tmp = a % b
                    a = b
                    b = tmp
                ret += '(' +\
                    str(eval(str(up) + '/' + str(a))).rstrip('0').rstrip('.') +\
                    ' / ' +\
                    str(eval(str(down) + '/' + str(a))).rstrip('0').rstrip('.') +\
                    ') = '
                print('\033[91m' + 'AAAAAAAAAAAAAAAAAAAAAAAA' + '\033[00m')
            else:
                print('\033[91m' + 'BBBBBBBBBBBBBBBBBBBBBBBB' + '\033[00m')
        return (ret + result)

    def calc(self):
        if (float(self.a) == 0):
            if (float(self.b) == 0):
                self.error = "This equation is false" if float(self.c) != 0 else "All answers are available"
            else:
                self.x1 = eval('(-' + self.c + ') / ' + self.b)
            return
        self.delta = str(eval(self.b + '**2-(4*' + self.a + '*' + self.c + ')'))
        if (float(self.delta) > 0):
            self.x1 = self.calc_and_irrationnal_fraction(str(-1 * float(self.b)), self.delta, self.a, '-')
            self.x2 = self.calc_and_irrationnal_fraction(str(-1 * float(self.b)), self.delta, self.a, '+')
        if (float(self.delta) == 0):
            self.x1 = eval('- ' + self.b + ' / (2 * ' + self.a + ')')
        if (float(self.delta) < 0):
            delta = str(-float(self.delta))
            self.x1 = ('(-' + self.b + ' - i * √(' + delta + ')) / (2*' + self.a + ')')
            self.x2 = ('(-' + self.b + ' + i * √(' + delta + ')) / (2*' + self.a + ')')

    def generate_object(self):
        # cut 2 sides of equation
        self.side_left = re.search("^(.+)=", self.eq, flags=re.MULTILINE).group(1)
        self.side_right = re.search("=(.+)$", self.eq, flags=re.MULTILINE).group(1)
        # parse all powers
        self.get_powers(self.side_right, self.power_right)
        self.get_powers(self.side_left, self.power_left)
        # parse all divided by X
        self.get_divided(self.side_left, "left")
        self.get_divided(self.side_right, "right")
        # merge 2 sides
        self.reduce_equation()
        # calc
        if (self.test_powers()):
            self.set_a_b_c()
            self.calc()
