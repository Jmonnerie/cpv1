regexes = {
    "exit": "QUIT|EXIT|Q|STOP|KILL",
    "goodChars": "^[0-9xX^* +/%\-\.\s]+\s*=\s*[0-9xX^* +/%\-\.\s]+$",
    "parseRegex": "(\d+(?:\.\d+)?[+\-/%*]\d(?:\.\d+)?)|(-?\d+(?:\.\d+)?(?:[\*\/]?X(?:\^-?\d*)?)?)",
        # ^(?:[\-+]?(\d+(?:\.\d+)?(?:[\*\/]?X(?:\^-?\d*)?)?))+=(?:[\-+]?(\d+(?:\.\d+)?(?:[\*\/]?X(?:\^-?\d*)?)?))+$
}

printPhrases = {
    "reducedForm": "Reduced Form:\t\t{{c}} * X^0 {{bSign}} {{bAbs}} * X^1 {{aSign}} {{aAbs}} * X^2 = 0'",
    "reducedNaturalForm": "Reduced Natural Form:\t{{a}}x^2 {{bSign}} {{bAbs}}x {{cSign}} {{c}} = 0",
    "polynomialDegree": "Polynomial Degree:\t{{polynomialDegree}}",
    "discriminant": "Discriminant:\t\t{{delta}}",
}